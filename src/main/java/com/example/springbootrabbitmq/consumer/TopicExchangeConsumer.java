package com.example.springbootrabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TopicExchangeConsumer {
    @RabbitListener(queues = "queue1")
    @RabbitHandler
    public void getQueue1Message(String  msg) {
        System.out.println("Queue 1 Receive [black.big.*, black.*.cat] , Msg: " + msg);
    }

    @RabbitListener(queues = "queue2")
    @RabbitHandler
    public void getQueue2Message(String msg) {
        System.out.println("Queue 2 Receive [*.small.*] , Msg: " + msg);
    }

    @RabbitListener(queues = "queue3")
    @RabbitHandler
    public void getQueue3Message(String msg) {
        System.out.println("Queue 3 Receive [#] , Msg: " + msg);
    }

}
