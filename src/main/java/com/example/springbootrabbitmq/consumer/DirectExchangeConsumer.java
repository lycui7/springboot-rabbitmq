package com.example.springbootrabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class DirectExchangeConsumer {
    @RabbitListener(queues = "queue4")
    @RabbitHandler
    public void getQueue1Message(String  msg) {
        System.out.println("Queue 4 Receive [big, small] , Msg: " + msg);
    }

    @RabbitListener(queues = "queue5")
    @RabbitHandler
    public void getQueue2Message(String msg) {
        System.out.println("Queue 5 Receive [cat] , Msg: " + msg);
    }
}
