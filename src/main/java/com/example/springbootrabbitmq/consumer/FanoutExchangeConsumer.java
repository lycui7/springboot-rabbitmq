package com.example.springbootrabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class FanoutExchangeConsumer {
    @RabbitListener(queues = "queue6")
    @RabbitHandler
    public void getQueue1Message(String  msg) {
        System.out.println("Queue 6 Receive , Msg: " + msg);
    }

    @RabbitListener(queues = "queue7")
    @RabbitHandler
    public void getQueue2Message(String msg) {
        System.out.println("Queue 7 Receive , Msg: " + msg);
    }
}
