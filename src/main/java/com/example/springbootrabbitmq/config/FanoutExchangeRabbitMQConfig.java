package com.example.springbootrabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FanoutExchangeRabbitMQConfig {
    public static final String fanoutExchangeName = "fanoutExchange1";

    // 声明扇形交换机
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(fanoutExchangeName);
    }

    // 声明消息队列
    @Bean
    public Queue messageQueue6() {
        return new Queue("queue6");
    }

    @Bean
    public Queue messageQueue7() {
        return new Queue("queue7");
    }

    // 向扇形交换机上绑定队列
    @Bean
    Binding bindingQueue6Exchange1(Queue messageQueue6, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind( messageQueue6 )
                .to( fanoutExchange );
    }

    @Bean
    Binding bindingQueue7Exchange(Queue messageQueue7, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind( messageQueue7 )
                .to( fanoutExchange );
    }
}
