package com.example.springbootrabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DirectExchangeRabbitMQConfig {

    public static final String directExchangeName = "directExchange1";

    private static final String queue4BindingKey1 = "big";
    private static final String queue4BindingKey2 = "small";
    private static final String queue5BindingKey =  "cat";

    // 声明直连交换机
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(directExchangeName);
    }

    // 声明消息队列
    @Bean
    public Queue messageQueue4() {
        return new Queue("queue4");
    }

    @Bean
    public Queue messageQueue5() {
        return new Queue("queue5");
    }

    // 向直连交换机上绑定队列
    @Bean
    Binding bindingQueue4Exchange1(Queue messageQueue4, DirectExchange directExchange) {
        return BindingBuilder.bind( messageQueue4 )
                .to( directExchange )
                .with( queue4BindingKey1 );
    }

    @Bean
    Binding bindingQueue4Exchange2(Queue messageQueue4, DirectExchange directExchange) {
        return BindingBuilder.bind( messageQueue4 )
                .to( directExchange )
                .with( queue4BindingKey2 );
    }

    @Bean
    Binding bindingQueue5Exchange(Queue messageQueue5, DirectExchange directExchange) {
        return BindingBuilder.bind( messageQueue5 )
                .to( directExchange )
                .with( queue5BindingKey );
    }
}
