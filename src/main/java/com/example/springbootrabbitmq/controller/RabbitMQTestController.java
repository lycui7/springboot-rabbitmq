package com.example.springbootrabbitmq.controller;

import com.example.springbootrabbitmq.producer.DirectExchangeProducer;
import com.example.springbootrabbitmq.producer.FanoutExchangeProducer;
import com.example.springbootrabbitmq.producer.TopicExchangeProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("testRabbitMQ")
public class RabbitMQTestController {
    @Autowired
    private TopicExchangeProducer topicExchangeProducer;

    @Autowired
    private DirectExchangeProducer directExchangeProducer;

    @Autowired
    private FanoutExchangeProducer fanoutExchangeProducer;

    @GetMapping("testTopicExchange")
    public String testTopicExchange() {
        // Test 1
        String routingKey1 = "black.albert.cui";
        topicExchangeProducer.send("testTopicExchange.test1", routingKey1);

        // Test 2
        String routingKey2 = "white.small.bo";
        topicExchangeProducer.send("testTopicExchange.test2", routingKey2);

        // Test 3
        String routingKey3 = "yellow.big.cat";
        topicExchangeProducer.send("testTopicExchange.test3", routingKey3);

        return "test TopicExchange success";
    }

    @GetMapping("testDirectExchange")
    public String testDirectExchange() {
        // Test 1
        String routingKey1 = "big";
        directExchangeProducer.send("testDirectExchange.test1", routingKey1);

        // Test 2
        String routingKey2 = "small";
        directExchangeProducer.send("testDirectExchange.test2", routingKey2);

        // Test 3
        String routingKey3 = "cat";
        directExchangeProducer.send("testDirectExchange.test3", routingKey3);

        return "test DirectExchange success";
    }

    @GetMapping("testFanoutExchange")
    public String testFanoutExchange() {
        // Test 1
        String routingKey1 = "big";
        fanoutExchangeProducer.send("testFanoutExchange.test1", routingKey1);

        // Test 2
        String routingKey2 = null;
        fanoutExchangeProducer.send("testFanoutExchange.test2", routingKey2);

        return "test FanoutExchange success";
    }

}
