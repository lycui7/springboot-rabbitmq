package com.example.springbootrabbitmq.producer;

import com.example.springbootrabbitmq.config.FanoutExchangeRabbitMQConfig;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FanoutExchangeProducer {
    @Autowired
    private AmqpTemplate rabbitMQTemplate;

    public void send(String msg, String routingKey) {
        rabbitMQTemplate.convertAndSend(FanoutExchangeRabbitMQConfig.fanoutExchangeName, routingKey, msg);
    }
}
